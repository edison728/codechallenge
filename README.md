# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A payslip csv file runner extracts data from CSV file which contains the annual income information of the employee, and then output the monthly tax, superannuation, gross income and net income.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up  
  Install jdk1.8 and compile all the java files. then run it from command line as following if the current directory is where the PayslipRunner class located:  
  java -cp .;supercsvDir/super-csv-2.4.0.jar PayslipRunner csvDir/payslips.csv  
  supercsvDir: This is the path of supercsv's jar file.  
  csvDir: This is the path of CSV file.	  
* Configuration  
  Please ensure the CSV column header matches exactly as following:  
  firstName,lastName,annualSalary,superRate,startDate
* Dependencies  
  super-csv-2.4.0.jar and junit 4.11 if unit tests are required to run.
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact