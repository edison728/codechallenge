import java.util.Date;
import java.util.Objects;

/**
 * The model class for the output detail of each employee
 *
 * Created by edison on 30/10/2015.
 */
public class EmployeeIncomeOutputData {

  /**
   * First name of the employee
   */
  private String firstName;

  /**
   * Last name of the employee
   */
  private String lastName;

  /**
   * The start date of the period
   */
  private Date startDate;

  /**
   * The end date of the period
   */
  private Date endDate;

  /**
   * The superannuation rate.
   */
  private int superAmount;

  /**
   * The gross income of the month
   */
  private int grossIncome;

  /**
   * The income tax of the month
   */
  private int incomeTax;

  /**
   * The net income of the month
   */
  private int netIncome;

  /**
   * Construct the employee with first name and last name.
   *
   * @param firstName First name of the employee
   * @param lastName Last name of the employee
   */
  public EmployeeIncomeOutputData(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * Get the first name
   *
   * @return String first name
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * Set the first name
   *
   * @param firstName String
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * Get the last name
   *
   * @return String last name
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * Set the last name
   *
   * @param lastName String
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * Get the start date
   *
   * @return Date startDate
   */
  public Date getStartDate() {
    return startDate;
  }

  /**
   * Set the start date
   *
   * @param startDate start date
   */
  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  /**
   * Get the end date
   *
   * @return Date endDate
   */
  public Date getEndDate() {
    return endDate;
  }

  /**
   * Set the end date
   *
   * @param endDate Date
   */
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  /**
   * Set the superannuation's payment
   *
   * @return int superAmount
   */
  public int getSuperAmount() {
    return superAmount;
  }

  /**
   * Get the superannuation's payment
   *
   * @param superAmount int
   */
  public void setSuperAmount(int superAmount) {
    this.superAmount = superAmount;
  }

  /**
   * Get the gross income of the employee
   *
   * @return long the gross income
   */
  public long getGrossIncome() {
    return grossIncome;
  }

  /**
   * Set the gross income of the employee
   *
   * @param grossIncome int
   */
  public void setGrossIncome(int grossIncome) {
    this.grossIncome = grossIncome;
  }

  /**
   * Get the income tax
   *
   * @return long
   */
  public int getIncomeTax() {
    return incomeTax;
  }

  /**
   * Set the income tax
   *
   * @param incomeTax int
   */
  public void setIncomeTax(int incomeTax) {
    this.incomeTax = incomeTax;
  }

  /**
   * Get the net income
   *
   * @return int
   */
  public long getNetIncome() {
    return netIncome;
  }

  /**
   * Set the net income
   *
   * @param netIncome long
   */
  public void setNetIncome(int netIncome) {
    this.netIncome = netIncome;
  }

  /**
   * Equals method for comparison
   *
   * @param o Object to compare
   * @return boolean True if both equal, otherwise, false.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EmployeeIncomeOutputData that = (EmployeeIncomeOutputData) o;
    return Objects.equals(superAmount, that.superAmount) &&
            Objects.equals(grossIncome, that.grossIncome) &&
            Objects.equals(incomeTax, that.incomeTax) &&
            Objects.equals(netIncome, that.netIncome) &&
            Objects.equals(firstName, that.firstName) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(startDate, that.startDate) &&
            Objects.equals(endDate, that.endDate);
  }

  /**
   * Return hashcode of the object
   *
   * @return int the hashcode
   */
  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, startDate, endDate, superAmount, grossIncome, incomeTax, netIncome);
  }
}
