/**
 * This class calculates the income.
 *
 * Created by edison on 29/10/2015.
 */
public final class IncomeCalculator {

  /**
   * Private constructor to stop user instantiation
   */
  private IncomeCalculator() {}

  /**
   * Get the gross monthly income from given annual salary
   *
   * @param annualSalary The total annual salary
   * @return long The gross monthly income
   */
  public static int getGrossMonthlyIncome(int annualSalary) {
    if (annualSalary < 0) {
      throw new IllegalArgumentException("Cannot calculate monthly gross income with annual salary " + annualSalary + ", must be greater than -1.");
    }
    double monthlyGrossIncome = annualSalary /(double) IncomeDeductionCalculator.NUMBER_OF_MONTH_PER_YEAR;
    return (int)Math.round(monthlyGrossIncome);
  }

  /**
   * Get the monthly net income from given annual salary
   *
   * @param annualSalary The total annual salary
   * @return long the net monthly income
   */
  public static int getMonthlyNetIncome(int annualSalary) {
    if (annualSalary < 0) {
      throw new IllegalArgumentException("Cannot calculate monthly net income with annual salary " + annualSalary + ", must be greater than -1.");
    }
    int monthlyGross = getGrossMonthlyIncome(annualSalary);
    int monthlyTaxDeduction = IncomeDeductionCalculator.calculateMonthlyIncomeTax(annualSalary);
    return (monthlyGross - monthlyTaxDeduction);
  }
}
