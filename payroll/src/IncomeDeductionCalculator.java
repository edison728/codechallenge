/**
 * The class calculates the monthly income deduction for the given annual salary
 * <p>
 * Created by edison on 29/10/2015.
 */
public final class IncomeDeductionCalculator {

  /**
   * Threshold to start paying tax.
   */
  public static final int FIRST_BAND_INCOME_THRESHOLD = 18201;

  /**
   * Threshold to pay $3,572 plus 32.5c for each $1
   */
  public static final int SECOND_BAND_INCOME_THRESHOLD = 37001;

  /**
   * Threshold to pay $17,547 plus 37c for each $1 over $80,000
   */
  public static final int THIRD_BAND_INCOME_THRESHOLD = 80001;

  /**
   * Threshold to pay $54,547 plus 45c for each $1 over $180,000
   */
  public static final int FOURTH_BAND_INCOME_THRESHOLD = 180001;

  /**
   * Assume 12 months per year
   */
  public static final int NUMBER_OF_MONTH_PER_YEAR = 12;

  /**
   * Private constructor to stop user to instantiate it
   */
  private IncomeDeductionCalculator() {}

  /**
   * Calculate the total amount of tax to pay in dollar.
   *
   * @param annualIncome Total annual income
   * @return total amount of tax in dollar.
   */
  public static final int calculateMonthlyIncomeTax(int annualIncome) {
    if (annualIncome < 0) {
      throw new IllegalArgumentException("Cannot calculate income tax with income: " +
              annualIncome + ", must be greater than 0.");
    }
    double monthlyTax = getYearlyIncomeTax(annualIncome) / NUMBER_OF_MONTH_PER_YEAR;
    return (int)Math.round(monthlyTax);
  }

  /**
   * Calculate the monthly superannuation from the annual income
   *
   * @param annualIncome The total income of the year.
   * @param percentage The weight percentage of the superannuation in the annual income
   * @return total amount of superannuation to pay per month
   */
  public static final int calculateMonthlySuperannuation(int annualIncome, double percentage) {
    if (annualIncome < 0) {
      throw new IllegalArgumentException("Cannot calculate superannuation with income:" +
              annualIncome + ", must be greater than 0.");
    }
    if (percentage > 1 || percentage < 0) {
      throw new IllegalArgumentException("Cannot calculate superannuation with weighted percentage: " + percentage +
      ", correct value should be 0 < super < 1");
    }
    double monthlyIncome = annualIncome / NUMBER_OF_MONTH_PER_YEAR;
    return (int)Math.round(monthlyIncome * percentage);
  }

  /**
   * Calculate the yearly income tax
   *
   * @param annualIncome The total income annually.
   * @return The yearly tax in dollar
   */
  private static final double getYearlyIncomeTax(int annualIncome) {
    double yearlyTax = 0;
    // 18201 <= income < 37001
    if (annualIncome < SECOND_BAND_INCOME_THRESHOLD && annualIncome >= FIRST_BAND_INCOME_THRESHOLD) {
      yearlyTax = (annualIncome - FIRST_BAND_INCOME_THRESHOLD) * 0.19;
    }
    // 37001 <= income < 80000
    else if (annualIncome < THIRD_BAND_INCOME_THRESHOLD && annualIncome >= SECOND_BAND_INCOME_THRESHOLD) {
      yearlyTax = 3572 + (annualIncome - SECOND_BAND_INCOME_THRESHOLD) * 0.325;
    }
    // 80001 <= income < 180000
    else if (annualIncome < FOURTH_BAND_INCOME_THRESHOLD && annualIncome >= THIRD_BAND_INCOME_THRESHOLD) {
      yearlyTax = 17547 + (annualIncome - THIRD_BAND_INCOME_THRESHOLD) * 0.37;
    }
    // 180000 <= income
    else if (annualIncome >= FOURTH_BAND_INCOME_THRESHOLD) {
      yearlyTax = 54547 + (annualIncome - FOURTH_BAND_INCOME_THRESHOLD) * 0.45;
    }
    return yearlyTax;
  }
}
