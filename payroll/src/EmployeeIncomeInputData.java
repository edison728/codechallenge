import java.util.Date;
import java.util.Objects;

/**
 * The model class for the input detail of each employee
 *
 * Created by edison on 31/10/2015.
 */
public class EmployeeIncomeInputData {

  /**
   * First name of the employee
   */
  private String firstName;

  /**
   * Last name of the employee
   */
  private String lastName;

  /**
   * The start date of the period
   */
  private Date startDate;

  /**
   * The annual income of the employee
   */
  private int annualSalary;

  /**
   * The super rate of the employee
   */
  private double superRate;

  /**
   * Default constuctor
   */
  public EmployeeIncomeInputData() {

  }

  /**
   * Construct the object with given first name and last name
   *
   * @param firstName First name
   * @param lastName Last name
   */
  public EmployeeIncomeInputData(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * Get the first name
   *
   * @return String first name
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * Set the first name
   *
   * @param firstName String
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * Get the last name
   *
   * @return String last name
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * Set the last name
   *
   * @param lastName String
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * Get the start date
   *
   * @return Date startDate
   */
  public Date getStartDate() {
    return startDate;
  }

  /**
   * Set the start date
   *
   * @param startDate start date
   */
  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  /**
   * Get the annual salary of the employee
   *
   * @return int annual salary
   */
  public int getAnnualSalary() {
    return annualSalary;
  }

  /**
   * Set the annual salary
   *
   * @param annualSalary int
   */
  public void setAnnualSalary(int annualSalary) {
    this.annualSalary = annualSalary;
  }

  /**
   * Get the super rate
   *
   * @return int super rate
   */
  public double getSuperRate() {
    return superRate;
  }

  /**
   * Set the super rate
   *
   * @param superRate double
   */
  public void setSuperRate(double superRate) {
    this.superRate = superRate;
  }

  /**
   * Equal method for comparison
   *
   * @param o Object to compare
   * @return boolean True if both equals, otherwise, false
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EmployeeIncomeInputData that = (EmployeeIncomeInputData) o;
    return Objects.equals(annualSalary, that.annualSalary) &&
            Objects.equals(superRate, that.superRate) &&
            Objects.equals(firstName, that.firstName) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(startDate, that.startDate);
  }

  /**
   * Return hashCode of the object
   *
   * @return int the hashcode
   */
  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, startDate, annualSalary, superRate);
  }
}
