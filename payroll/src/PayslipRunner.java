import java.text.SimpleDateFormat;
import java.util.List;

/**
 * The class to invoke the income data input and output process.
 *
 * Created by edison on 30/10/2015.
 */
public class PayslipRunner {

  /**
   * The output value delimiter
   */
  private static final char VALUE_DELIMITER = ',';

  /**
   * The output date format
   */
  private static final String OUTPUT_DATE_FORMAT = "dd MMMM";

  /**
   * Run the data processing based on the given csv file
   *
   * @param args The argument contains the csv file directory
   * @throws Exception If any problem occurs
   */
  public List<EmployeeIncomeOutputData> run(String[] args) throws Exception {
    if (args != null && args.length > 1) {
      throw new UnsupportedOperationException("Invalid command line parameter, only one parameter is supported.");
    }

    if (args == null || args.length < 1) {
      throw new IllegalArgumentException("CSV file path must be specified.");
    }

    CsvDataHandler csvDataHandler = new CsvDataHandler(args[0]);
    List<EmployeeIncomeOutputData> outputDataList = csvDataHandler.getData();
    StringBuilder dataOutput = new StringBuilder();
    SimpleDateFormat dateFormatter = new SimpleDateFormat(OUTPUT_DATE_FORMAT);
    if (outputDataList.size() > 0) {
      System.out.println("Process starts:");
      System.out.println("name, pay period, gross income, income tax, net income, super");
      for (EmployeeIncomeOutputData everyRow : outputDataList) {
        String displayStartDate = dateFormatter.format(everyRow.getStartDate());
        String displayEndDate = dateFormatter.format(everyRow.getEndDate());
        dataOutput.append(everyRow.getFirstName()).append(" ").append(everyRow.getLastName())
                .append(VALUE_DELIMITER)
                .append(displayStartDate).append("-").append(displayEndDate)
                .append(VALUE_DELIMITER)
                .append(everyRow.getGrossIncome())
                .append(VALUE_DELIMITER)
                .append(everyRow.getIncomeTax())
                .append(VALUE_DELIMITER)
                .append(everyRow.getNetIncome())
                .append(VALUE_DELIMITER)
                .append(everyRow.getSuperAmount());

        System.out.println(dataOutput.toString());
        dataOutput.delete(0, dataOutput.length());
      }
      System.out.println("Process completed successfully!");
    }
    return outputDataList;
  }

  /**
   * The main method to start the process
   *
   * @param args Array of arguments from command line
   */
  public static void main(String[] args) {
    try {
      new PayslipRunner().run(args);
    }
    catch (Exception ex) {
      System.out.println("Error occurred: " + ex.getMessage());
      System.exit(1);
    }
  }
}
