import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The class to extract data from the csv file from the file system
 * following the format first row is column headers, data starts from 2nd
 * row.
 *
 * firstName,lastName,annualSalary,superRate,startDate
 * David,Rudd,60050,9,01 March
 *
 * Created by edison on 30/10/2015.
 */
public class CsvDataHandler {

  /**
   * Date format from CSV file
   */
  private static final String DATE_FORMAT = "dd MMM";

  /**
   * The initial size of the data list
   */
  private final static int INITIAL_SIZE = 1000;

  /**
   * List of {@link EmployeeIncomeInputData}s extracted from csv file
   */
  private List<EmployeeIncomeOutputData> incomeData;

  /**
   * The csv file name to be extracted from
   */
  private String fileToExtract;

  /**
   * Construct the data handler with the CSV file path
   *
   * @param fileToExtract CSV file
   */
  public CsvDataHandler(String fileToExtract) {
    this.fileToExtract = fileToExtract;
    this.incomeData = new ArrayList<EmployeeIncomeOutputData>(INITIAL_SIZE);
  }

  /**
   * Get a list of {@link EmployeeIncomeOutputData}s
   *
   * @return List of {@link EmployeeIncomeOutputData}
   */
  public List<EmployeeIncomeOutputData> getData() throws Exception {
    ICsvBeanReader beanReader = null;
    try {
      beanReader = new CsvBeanReader(new FileReader(fileToExtract), CsvPreference.STANDARD_PREFERENCE);
      final String[] columnHeaders = beanReader.getHeader(true);
      final CellProcessor[] cellProcessors = getCellProcessors();

      EmployeeIncomeInputData employeeIncomeInputData;
      // Binding the input data bean with the row in the csv file
      while ((employeeIncomeInputData = beanReader.read(EmployeeIncomeInputData.class, columnHeaders, cellProcessors)) != null) {
        String firstName = employeeIncomeInputData.getFirstName();
        String lastName = employeeIncomeInputData.getLastName();

        EmployeeIncomeOutputData outputData = new EmployeeIncomeOutputData(firstName,lastName);
        double superRate = employeeIncomeInputData.getSuperRate() / 100.0;
        int annualIncome = employeeIncomeInputData.getAnnualSalary();
        Date startDate = employeeIncomeInputData.getStartDate();
        // Populate all output data
        outputData.setSuperAmount(IncomeDeductionCalculator.calculateMonthlySuperannuation(annualIncome, superRate));
        outputData.setNetIncome(IncomeCalculator.getMonthlyNetIncome(annualIncome));
        outputData.setIncomeTax(IncomeDeductionCalculator.calculateMonthlyIncomeTax(annualIncome));
        outputData.setGrossIncome(IncomeCalculator.getGrossMonthlyIncome(annualIncome));
        Date firstDayOfMonth = findFirstDayOfMonth(startDate);
        outputData.setStartDate(firstDayOfMonth);
        outputData.setEndDate(findEndDate(firstDayOfMonth));
        incomeData.add(outputData);
      }
    }
    finally {
      // Close the bean reader no matter what happens
      if (beanReader != null) {
        beanReader.close();
      }
    }

    return incomeData;
  }

  /**
   * The payment start date for the employee
   *
   * @param startDate Date
   * @return Date the first day of the month
   */
  private Date findFirstDayOfMonth(Date startDate) {
    Calendar c = Calendar.getInstance();
    c.setTime(startDate);
    // Use 2013 as year sample
    c.set(Calendar.YEAR, 2013);
    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
    return c.getTime();
  }

  /**
   * Find the date of the calendar month from given start date
   *
   * @param startDate Given start date
   * @return Date endDate
   */
  private Date findEndDate(Date startDate) {
    Calendar c = Calendar.getInstance();
    c.setTime(startDate);
    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
    return c.getTime();
  }

  /**
   * Predefined csv cell processor
   *
   * @return array of cell processors
   */
  private CellProcessor[] getCellProcessors() {
    final CellProcessor[] processors = new CellProcessor[] {
            new NotNull(), // first name
            new NotNull(), // last name
            new ParseInt(), // annual salary
            new ParseDouble(), // super rate
            new ParseDate(DATE_FORMAT) // payment start date
    };
    return processors;
  };
}
