import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test cases for CSV data handler
 *
 * Created by edison on 30/10/2015.
 */
public class CsvDataHandlerTest {

  @Test (expected = FileNotFoundException.class)
  public void test_parse_file_not_existed() throws Exception {
    CsvDataHandler csvParser = new CsvDataHandler("testFile.csv");
    List<EmployeeIncomeOutputData> dataList = csvParser.getData();
  }

  @Test
  public void test_parse_file() throws Exception {
    URL url = this.getClass().getResource("/testFile.csv");
    File testCsv = new File(url.getFile());
    CsvDataHandler csvParser = new CsvDataHandler(testCsv.getAbsolutePath());
    List<EmployeeIncomeOutputData> dataList = csvParser.getData();
    assertEquals(6, dataList.size());
    EmployeeIncomeOutputData output01 = dataList.get(0);
    assertEquals("David", output01.getFirstName());
    assertEquals("Rudd", output01.getLastName());
    assertEquals(5004,output01.getGrossIncome());
    assertEquals(922, output01.getIncomeTax());
    assertEquals(4082, output01.getNetIncome());
    assertEquals(450, output01.getSuperAmount());
    assertEquals(makeDate("01/03/2013"), output01.getStartDate());
    assertEquals(makeDate("31/03/2013"), output01.getEndDate());

    EmployeeIncomeOutputData output02 = dataList.get(1);
    assertEquals("Ryan", output02.getFirstName());
    assertEquals("Chen", output02.getLastName());
    assertEquals(10000,output02.getGrossIncome());
    assertEquals(2696, output02.getIncomeTax());
    assertEquals(7304, output02.getNetIncome());
    assertEquals(1000, output02.getSuperAmount());
    assertEquals(makeDate("01/03/2013"), output02.getStartDate());
    assertEquals(makeDate("31/03/2013"), output02.getEndDate());
    // Verify the test data when it's requested for February
    EmployeeIncomeOutputData output03 = dataList.get(2);
    assertEquals("Ian", output03.getFirstName());
    assertEquals("Collins", output03.getLastName());
    assertEquals(417,output03.getGrossIncome());
    assertEquals(0, output03.getIncomeTax());
    assertEquals(417, output03.getNetIncome());
    assertEquals(40, output03.getSuperAmount());
    assertEquals(makeDate("01/02/2013"), output03.getStartDate());
    assertEquals(makeDate("28/02/2013"), output03.getEndDate());
    // Verify the test data when it's requested at the end of December
    EmployeeIncomeOutputData output04 = dataList.get(3);
    assertEquals("Helen", output04.getFirstName());
    assertEquals("Hunt", output04.getLastName());
    assertEquals(3083,output04.getGrossIncome());
    assertEquals(298, output04.getIncomeTax());
    assertEquals(2785, output04.getNetIncome());
    assertEquals(299, output04.getSuperAmount());
    assertEquals(makeDate("01/12/2013"), output04.getStartDate());
    assertEquals(makeDate("31/12/2013"), output04.getEndDate());
    // Verify the test data when it's requested at the beginning of January
    EmployeeIncomeOutputData output05 = dataList.get(4);
    assertEquals("Dick", output05.getFirstName());
    assertEquals("Smith", output05.getLastName());
    assertEquals(16250,output05.getGrossIncome());
    assertEquals(5108, output05.getIncomeTax());
    assertEquals(11142, output05.getNetIncome());
    assertEquals(1463, output05.getSuperAmount());
    assertEquals(makeDate("01/01/2013"), output05.getStartDate());
    assertEquals(makeDate("31/01/2013"), output05.getEndDate());

    EmployeeIncomeOutputData output06 = dataList.get(5);
    assertEquals("Lucy", output06.getFirstName());
    assertEquals("Lawrence", output06.getLastName());
    assertEquals(0,output06.getGrossIncome());
    assertEquals(0, output06.getIncomeTax());
    assertEquals(0, output06.getNetIncome());
    assertEquals(0, output06.getSuperAmount());
    assertEquals(makeDate("01/07/2013"), output06.getStartDate());
    assertEquals(makeDate("31/07/2013"), output06.getEndDate());
  }

  @Test
  public void test_empty_data_file() throws Exception {
    URL url = this.getClass().getResource("/emptyDataFile.csv");
    File testCsv = new File(url.getFile());
    CsvDataHandler csvParser = new CsvDataHandler(testCsv.getAbsolutePath());
    List<EmployeeIncomeOutputData> output = csvParser.getData();
    assertTrue(output.size() == 0);
  }

  private Date makeDate(String dateString) throws Exception {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    return formatter.parse(dateString);
  }
}
