import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test cases for {@link IncomeDeductionCalculator}
 *
 * Created by edison on 29/10/2015.
 */
public class IncomeDeductionCalculatorTest {

  @Test (expected = IllegalArgumentException.class)
  public void testInvalidInput() {
    IncomeDeductionCalculator.calculateMonthlyIncomeTax(-1);
  }

  @Test
  public void test_income_between_0_and_37000() {
    assertEquals(0, IncomeDeductionCalculator.calculateMonthlyIncomeTax(0));
    assertEquals(0, IncomeDeductionCalculator.calculateMonthlyIncomeTax(9200));
    assertEquals(0, IncomeDeductionCalculator.calculateMonthlyIncomeTax(18200));
    // Rounding to 0 dollar here
    assertEquals(0, IncomeDeductionCalculator.calculateMonthlyIncomeTax(18201));
    assertEquals(108, IncomeDeductionCalculator.calculateMonthlyIncomeTax(25000));
    assertEquals(298, IncomeDeductionCalculator.calculateMonthlyIncomeTax(37000));
  }

  @Test
  public void test_income_between_37001_and_80000() {
    assertEquals(298, IncomeDeductionCalculator.calculateMonthlyIncomeTax(37001));
    assertEquals(922, IncomeDeductionCalculator.calculateMonthlyIncomeTax(60050));
    assertEquals(1462, IncomeDeductionCalculator.calculateMonthlyIncomeTax(80000));
  }

  @Test
  public void test_income_over_80000() {
    assertEquals(1462, IncomeDeductionCalculator.calculateMonthlyIncomeTax(80001));
    assertEquals(1925, IncomeDeductionCalculator.calculateMonthlyIncomeTax(95000));
    assertEquals(4546, IncomeDeductionCalculator.calculateMonthlyIncomeTax(180000));
  }

  @Test
  public void test_income_over_180000() {
    assertEquals(4546, IncomeDeductionCalculator.calculateMonthlyIncomeTax(180001));
    assertEquals(5296, IncomeDeductionCalculator.calculateMonthlyIncomeTax(200000));
  }

  @Test (expected = IllegalArgumentException.class)
  public void test_invalid_income_superannuation() {
    IncomeDeductionCalculator.calculateMonthlySuperannuation(-1, 0.4);
  }

  @Test (expected = IllegalArgumentException.class)
  public void test_invalid_percentage_greater_than_one() {
    IncomeDeductionCalculator.calculateMonthlySuperannuation(1000, 1.2);
  }

  @Test (expected = IllegalArgumentException.class)
  public void test_invalid_percentage_smaller_than_zero() {
    IncomeDeductionCalculator.calculateMonthlySuperannuation(1000, -0.5);
  }

  @Test
  public void test_superannuation_calculation() {
    assertEquals(0, IncomeDeductionCalculator.calculateMonthlySuperannuation(0, 0.09));
    assertEquals(450, IncomeDeductionCalculator.calculateMonthlySuperannuation(60050, 0.09));
    assertEquals(633, IncomeDeductionCalculator.calculateMonthlySuperannuation(80000, 0.095));
  }
}
