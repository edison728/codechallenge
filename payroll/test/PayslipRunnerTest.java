import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test cases for payslip runner
 *
 * Created by edison on 1/11/2015.
 */
public class PayslipRunnerTest {

  @Test (expected = UnsupportedOperationException.class)
  public void test_too_many_args_specified() throws Exception {
    PayslipRunner runner = new PayslipRunner();
    runner.run(new String[] {"dir1", "dir2"});
  }

  @Test (expected = IllegalArgumentException.class)
  public void test_no_arg_specified() throws Exception {
    PayslipRunner runner = new PayslipRunner();
    runner.run(new String[]{});
  }

  @Test
  public void test_run() throws Exception {
    URL url = this.getClass().getResource("/testFile.csv");
    File testCsv = new File(url.getFile());
    PayslipRunner runner = new PayslipRunner();
    List<EmployeeIncomeOutputData> result = runner.run(new String[]{testCsv.getAbsolutePath()});
    assertEquals(6, result.size());
  }

  @Test
  public void test_empty_data_file() throws Exception {
    URL url = this.getClass().getResource("/emptyDataFile.csv");
    File testCsv = new File(url.getFile());
    PayslipRunner runner = new PayslipRunner();
    List<EmployeeIncomeOutputData> result = runner.run(new String[]{testCsv.getAbsolutePath()});
    assertEquals(0, result.size());
  }
}
