import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Test cases for {@link EmployeeIncomeInputData}
 *
 * Created by edison on 31/10/2015.
 */
public class EmployeeIncomeInputDataTest {

  @Test
  public void test_outputDetail_constructor() {
    EmployeeIncomeInputData employeeIncomeInputData = new EmployeeIncomeInputData("david", "rudd");
    assertEquals("david", employeeIncomeInputData.getFirstName());
    assertEquals("rudd", employeeIncomeInputData.getLastName());
  }

  @Test
  public void test_setter_getter() throws Exception {
    EmployeeIncomeInputData employeeIncomeInputData = new EmployeeIncomeInputData("Ryan", "Chen");
    employeeIncomeInputData.setFirstName("Alen");
    employeeIncomeInputData.setLastName("Clark");
    assertEquals("Alen", employeeIncomeInputData.getFirstName());
    assertEquals("Clark", employeeIncomeInputData.getLastName());
    employeeIncomeInputData.setAnnualSalary(1100);
    assertEquals(1100, employeeIncomeInputData.getAnnualSalary());
    employeeIncomeInputData.setSuperRate(15);
    assertEquals("Super rate is incorrect", 15, employeeIncomeInputData.getSuperRate() , 0.001);
    Date start = makeDate("03/05/2014");
    employeeIncomeInputData.setStartDate(start);
    assertEquals(start, employeeIncomeInputData.getStartDate());
  }

  @Test
  public void test_equals_method() throws Exception {
    EmployeeIncomeInputData employeeIncomeInputData1 = new EmployeeIncomeInputData("Alan", "Smith");
    EmployeeIncomeInputData employeeIncomeInputData2 = null;
    assertFalse(employeeIncomeInputData1.equals(employeeIncomeInputData2));
    employeeIncomeInputData2 = new EmployeeIncomeInputData("Alan", "Smith");
    assertEquals(employeeIncomeInputData1, employeeIncomeInputData2);
    assertEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());

    employeeIncomeInputData1.setSuperRate(10);
    assertFalse(employeeIncomeInputData1.equals(employeeIncomeInputData2));
    assertNotEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());
    employeeIncomeInputData2.setSuperRate(9);
    assertFalse(employeeIncomeInputData1.equals(employeeIncomeInputData2));
    assertNotEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());
    employeeIncomeInputData2.setSuperRate(10);
    assertEquals(employeeIncomeInputData1, employeeIncomeInputData2);
    assertEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());

    employeeIncomeInputData1.setAnnualSalary(55000);
    assertFalse(employeeIncomeInputData1.equals(employeeIncomeInputData2));
    assertNotEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());
    employeeIncomeInputData2.setAnnualSalary(60054);
    assertFalse(employeeIncomeInputData1.equals(employeeIncomeInputData2));
    assertNotEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());
    employeeIncomeInputData2.setAnnualSalary(55000);
    assertEquals(employeeIncomeInputData1, employeeIncomeInputData2);
    assertEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());


    Date start = makeDate("03/05/2014");
    employeeIncomeInputData1.setStartDate(start);
    assertFalse(employeeIncomeInputData1.equals(employeeIncomeInputData2));
    assertNotEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());
    employeeIncomeInputData2.setStartDate(makeDate("04/05/2014"));
    assertFalse(employeeIncomeInputData1.equals(employeeIncomeInputData2));
    assertNotEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());
    employeeIncomeInputData2.setStartDate(start);
    assertEquals(employeeIncomeInputData1, employeeIncomeInputData2);
    assertEquals(employeeIncomeInputData1.hashCode(), employeeIncomeInputData2.hashCode());
  }

  private Date makeDate(String dateString) throws Exception {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    return formatter.parse(dateString);
  }
}
