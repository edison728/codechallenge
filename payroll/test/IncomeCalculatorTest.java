import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test cases for the {@link IncomeCalculator}
 *
 * Created by edison on 29/10/2015.
 */
public class IncomeCalculatorTest {

  private IncomeCalculator incomeCalculator;

  @Test (expected = IllegalArgumentException.class)
  public void testGetGrossSalary_invalid_salary() {
    IncomeCalculator.getGrossMonthlyIncome(-100);
  }

  @Test
  public void test_GetGrossMonthlyIncome() {
    assertEquals(5004, IncomeCalculator.getGrossMonthlyIncome(60050));
    assertEquals(0, IncomeCalculator.getMonthlyNetIncome(0));
    assertEquals(15000, IncomeCalculator.getGrossMonthlyIncome(180000));
  }

  @Test (expected = IllegalArgumentException.class)
  public void test_GetMonthlyNetIncome_invalid_salary() {
    IncomeCalculator.getMonthlyNetIncome(-100);
  }

  @Test
  public void test_GetMonthlyNetIncome() {
    assertEquals(4082, IncomeCalculator.getMonthlyNetIncome(60050));
    assertEquals(0, IncomeCalculator.getMonthlyNetIncome(0));
  }
}
