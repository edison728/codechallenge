import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by edison on 30/10/2015.
 */
public class EmployeeIncomeOutputDataTest {

  @Test
  public void test_outputDetail_constructor() {
    EmployeeIncomeOutputData employeeIncomeOutputData = new EmployeeIncomeOutputData("david", "rudd");
    assertEquals("david", employeeIncomeOutputData.getFirstName());
    assertEquals("rudd", employeeIncomeOutputData.getLastName());
    assertNull(employeeIncomeOutputData.getEndDate());
    assertNull(employeeIncomeOutputData.getStartDate());
    assertEquals(0, employeeIncomeOutputData.getGrossIncome());
    assertEquals(0, employeeIncomeOutputData.getNetIncome());
    assertEquals(0, employeeIncomeOutputData.getIncomeTax());
  }

  @Test
  public void test_setter_getter() throws Exception {
    EmployeeIncomeOutputData employeeIncomeOutputData = new EmployeeIncomeOutputData("Ryan", "Chen");
    employeeIncomeOutputData.setFirstName("Alen");
    employeeIncomeOutputData.setLastName("Clark");
    assertEquals("Alen", employeeIncomeOutputData.getFirstName());
    assertEquals("Clark", employeeIncomeOutputData.getLastName());
    employeeIncomeOutputData.setGrossIncome(1100);
    assertEquals(1100, employeeIncomeOutputData.getGrossIncome());
    employeeIncomeOutputData.setNetIncome(5647);
    assertEquals(5647, employeeIncomeOutputData.getNetIncome());
    employeeIncomeOutputData.setIncomeTax(875);
    assertEquals(875, employeeIncomeOutputData.getIncomeTax());
    Date start = makeDate("03/05/2014");
    Date end = makeDate("10/05/2014");
    employeeIncomeOutputData.setStartDate(start);
    employeeIncomeOutputData.setEndDate(end);
    assertEquals(start, employeeIncomeOutputData.getStartDate());
    assertEquals(end, employeeIncomeOutputData.getEndDate());
    employeeIncomeOutputData.setSuperAmount(564);
    assertEquals(564, employeeIncomeOutputData.getSuperAmount());
  }

  @Test
  public void test_equals_method() throws Exception {
    EmployeeIncomeOutputData employeeIncomeOutputData1 = new EmployeeIncomeOutputData("Alan", "Smith");
    EmployeeIncomeOutputData employeeIncomeOutputData2 = null;
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    employeeIncomeOutputData2 = new EmployeeIncomeOutputData("Alan", "Smith");
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());

    employeeIncomeOutputData1.setIncomeTax(1100);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setIncomeTax(564);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setIncomeTax(1100);
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());

    employeeIncomeOutputData1.setNetIncome(3577);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setNetIncome(5478);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setNetIncome(3577);
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());

    employeeIncomeOutputData1.setGrossIncome(6789);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setGrossIncome(3343);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setGrossIncome(6789);
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());

    employeeIncomeOutputData1.setSuperAmount(655);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setSuperAmount(654);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setSuperAmount(655);
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());

    employeeIncomeOutputData1.setIncomeTax(987);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setIncomeTax(1002);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setIncomeTax(987);
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());

    Date start = makeDate("03/05/2014");
    employeeIncomeOutputData1.setStartDate(start);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setStartDate(makeDate("04/05/2014"));
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setStartDate(start);
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());

    Date end = makeDate("10/05/2014");
    employeeIncomeOutputData1.setEndDate(end);
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setEndDate(makeDate("11/05/2014"));
    assertFalse(employeeIncomeOutputData1.equals(employeeIncomeOutputData2));
    assertNotEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
    employeeIncomeOutputData2.setEndDate(end);
    assertEquals(employeeIncomeOutputData1, employeeIncomeOutputData2);
    assertEquals(employeeIncomeOutputData1.hashCode(), employeeIncomeOutputData2.hashCode());
  }

  private Date makeDate(String dateString) throws Exception {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    return formatter.parse(dateString);
  }
}
